<?php

namespace App\Tests\Services\Helpers\LectorCSV;

use App\Services\Helpers\LectorCSV\LectorCSVPHP;
use PHPUnit\Framework\TestCase;

class LectorCSVPHPTest extends TestCase
{
    public function testLecturaCSVPHP(): void
    {
        $rutaFicheroPrueba = "public/test.csv";
        $contenidoPrueba = array("1", "Colombia", "Grupo A");

        //Se prepara un fichero csv de prueba
        $file = fopen($rutaFicheroPrueba, 'w');
        fputcsv($file, $contenidoPrueba);
        fclose($file);

        //Se realiza la lectura
        $lectorCSV = new LectorCSVPHP();
        $contenidoLeido = $lectorCSV->leerCSV($rutaFicheroPrueba, false)[0];

        self::assertEquals($contenidoPrueba, $contenidoLeido);
    }
}
