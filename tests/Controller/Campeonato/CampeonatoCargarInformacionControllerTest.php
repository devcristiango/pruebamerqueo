<?php

namespace App\Tests\Controller\Campeonato;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CampeonatoCargarInformacionControllerTest extends WebTestCase
{

    public function testIndexAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/campeonato/index');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Campeonato Mundial - Prueba Merqueo');
    }

    public function testCargarInformacionAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/campeonato/cargar');
        $this->assertResponseIsSuccessful();
    }
}
