<?php

namespace App\Tests\Controller\Campeonato;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CampeonatoTablaPosicionesControllerTest extends WebTestCase
{
    public function testIndex()
    {
        //Es necesario verificar que se hayan cargado datos y simulado
        $client = static::createClient();
        $crawler = $client->request('GET', '/campeonato/cargar');
        $this->assertResponseIsSuccessful();

        $crawler = $client->request('GET', '/campeonato/simular');
        $this->assertResponseIsSuccessful();

        $crawler = $client->request('GET', '/campeonato/simular');
        $this->assertResponseIsSuccessful();
    }
}
