<?php

namespace App\Tests\Controller\Campeonato;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CampeonatoSimularControllerTest extends WebTestCase
{
    public function testSimularEjecucionCampeonato()
    {
        //Es necesario verificar que se hayan cargado datos
        $client = static::createClient();
        $crawler = $client->request('GET', '/campeonato/cargar');
        $this->assertResponseIsSuccessful();

        $crawler = $client->request('GET', '/campeonato/simular');
        $this->assertResponseIsSuccessful();
    }
}
