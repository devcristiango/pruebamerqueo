<?php

namespace App\UseCases\Grupo;

use App\Entity\Equipo;
use App\Entity\Grupo;
use App\Repository\Grupo\IGrupoRepository;

class ListarInformacionEquiposInscritosUseCase
{
    private $repository;

    public function __construct(IGrupoRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Este metodo retorna la información de los equipos inscritos
     * en cada grupo.
     *
     * @return array
     * @noinspection PhpMethodParametersCountMismatchInspection
     */
    public function __invoke(): ?array
    {
        $respuesta = array();

        try {
            $grupos = $this->repository->findAllGrupos(false);

            /** @var Grupo $grupo */
            foreach ($grupos as $grupo) {
                $grupoArray = array(
                    "grupoId" => $grupo->getId(),
                    "grupoNombre" => $grupo->getNombre()
                );

                /** @var Equipo $equipo */
                foreach ($grupo->getEquipos() as $equipo) {
                    $equipoArray = array(
                        "equipoId" => $equipo->getId(),
                        "equipoNombre" => $equipo->getNombre(),
                        "equipoBandera" => $equipo->getBandera()
                    );
                    array_push($grupoArray, $equipoArray);
                }

                array_push($respuesta, $grupoArray);
            }

            return $respuesta;
        } catch (\Throwable $th) {
            return null;
        }
    }
}