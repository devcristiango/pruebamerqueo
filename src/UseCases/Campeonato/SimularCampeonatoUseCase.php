<?php

namespace App\UseCases\Campeonato;

ini_set('max_execution_time', '300');
set_time_limit(300);

use App\Entity\Campeonato;
use App\Entity\Equipo;
use App\Entity\Grupo;
use App\Entity\ResultadoPartido;
use App\Repository\Campeonato\ICampeonatoRepository;
use App\Repository\Equipo\IEquipoRepository;
use App\Repository\Gol\IGolRepository;
use App\Repository\Grupo\IGrupoRepository;
use App\Repository\Partido\IPartidoRepository;
use App\Repository\ResultadoPartido\IResultadoPartidoRepository;
use App\Repository\Tarjeta\ITarjetaRepository;
use phpDocumentor\Reflection\Types\Self_;

class SimularCampeonatoUseCase
{
    /** @var ICampeonatoRepository */
    private $campeonatoRepository;

    /** @var IEquipoRepository */
    private $equipoRepository;

    /** @var IGolRepository */
    private $golRepository;

    /** @var IGrupoRepository */
    private $grupoRepository;

    /** @var IPartidoRepository */
    private $partidoRepository;

    /** @var IResultadoPartidoRepository */
    private $resultadoPartidoRepository;

    /** @var ITarjetaRepository */
    private $tarjetaRepository;

    /** @var string Llave para array de resultados */
    const CLAVE_ARRAY_GANADORES = 'ganadores';

    /** @var string Llave para array de resultados */
    const CLAVE_ARRAY_PERDEDORES = 'perdedores';

    /** @var string para la creacion de tarjetas amarillas */
    const TARJETA_AMARRILLA = 'amarilla';

    /** @var string para la creacion de tarjetas rojas */
    const TARJETA_ROJA = 'roja';

    /** @var string para definir la fase del partido */
    const FASE_GRUPOS = 'grupos';

    /** @var string para definir la fase del partido */
    const FASE_OCTAVOS = 'octavos';

    /** @var string para definir la fase del partido */
    const FASE_CUARTOS = 'cuartos';

    /** @var string para definir la fase del partido */
    const FASE_SEMIFINAL = 'semifinal';

    /** @var string para definir la fase del partido */
    const FASE_FINAL = 'final';

    public function __construct(
        ICampeonatoRepository       $campeonatoRepository,
        IEquipoRepository           $equipoRepository,
        IGolRepository              $golRepository,
        IGrupoRepository            $grupoRepository,
        IPartidoRepository          $partidoRepository,
        IResultadoPartidoRepository $resultadoPartidoRepository,
        ITarjetaRepository          $tarjetaRepository
    ) {
        $this->campeonatoRepository = $campeonatoRepository;
        $this->equipoRepository = $equipoRepository;
        $this->golRepository = $golRepository;
        $this->grupoRepository = $grupoRepository;
        $this->partidoRepository = $partidoRepository;
        $this->resultadoPartidoRepository = $resultadoPartidoRepository;
        $this->tarjetaRepository = $tarjetaRepository;
    }

    /**
     * Esta función se encarga de realizar la simulación del mundial siguiendo las
     * reglas establecidas para el torneo. Retorna un array con la información de
     * los encuentros y partidos.
     *
     * @return array|null
     * @noinspection PhpMethodParametersCountMismatchInspection
     */
    public function __invoke(): ?array
    {
        try {
            //Arreglos con los resultados del encuentro
            $resultadosSimulacion = array();

            //Se crea un campeonato
            $campeonatoNombre = "Copa Mundo";
            $campeonato = $this->campeonatoRepository->save($campeonatoNombre);

            if (!isset($campeonato)) {
                return null;
            }

            //Se inicia el torneo fase de grupos
            $grupos = $this->grupoRepository->findAllGrupos();
            $resultadosFaseDeGrupos = $this->jugarFaseDeGrupos($grupos, $campeonato);
            array_push($resultadosSimulacion, $resultadosFaseDeGrupos);

            $resultadosOctavosFinal = $this->jugarOctavosFinal($resultadosFaseDeGrupos, $campeonato);
            array_push($resultadosSimulacion, $resultadosOctavosFinal);

            $resultadosCuartosFinal = $this->jugarCuartosFinal($resultadosOctavosFinal, $campeonato);
            array_push($resultadosSimulacion, $resultadosCuartosFinal);

            $resultadosSemifinal = $this->jugarSemifinal($resultadosCuartosFinal, $campeonato);
            array_push($resultadosSimulacion, $resultadosSemifinal);

            $resultadosFinal = $this->jugarFinal($resultadosSemifinal, $campeonato);
            array_push($resultadosSimulacion, $resultadosFinal);

            return $resultadosSimulacion;

        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * Esta función simula los encuentros de octavos de final en donde los clasificados
     * de los equipos se encuentran.
     * Se realiza de la siguiente manera:
     * - 1ro grupo X - 2do grupo X+1
     * - 2do grupo X - 1ro grupo X+1
     *
     * @param array $resultadosFaseDeGrupos array con los datos ordenados de los equipos clasificados
     * @param Campeonato $campeonato campeonato en juego
     * @return array array con la información de los ganadores y perdedores de octavos
     */
    public function jugarOctavosFinal(array  $resultadosFaseDeGrupos, Campeonato $campeonato) : array
    {
        $resultadosOctavosFinal = array();

        for ($i=1; $i <= 8; $i = 2 + $i) {
            $idEquipoLocal     = $resultadosFaseDeGrupos[$i]["resultadoEquipos"][0]["idEquipo"];
            $equipoLocal       = $this->equipoRepository->findById($idEquipoLocal);
            $idEquipoVisitante = $resultadosFaseDeGrupos[$i+1]["resultadoEquipos"][1]["idEquipo"];
            $equipoVisitante   = $this->equipoRepository->findById($idEquipoVisitante);
            $resultado = $this->jugarPartido(self::FASE_OCTAVOS, $equipoLocal, $equipoVisitante, $campeonato);
            array_push($resultadosOctavosFinal, $resultado);

            $idEquipoLocal     = $resultadosFaseDeGrupos[$i]["resultadoEquipos"][1]["idEquipo"];
            $equipoLocal       = $this->equipoRepository->findById($idEquipoLocal);
            $idEquipoVisitante = $resultadosFaseDeGrupos[$i+1]["resultadoEquipos"][0]["idEquipo"];
            $equipoVisitante   = $this->equipoRepository->findById($idEquipoVisitante);
            $resultado = $this->jugarPartido(self::FASE_OCTAVOS, $equipoLocal, $equipoVisitante, $campeonato);
            array_push($resultadosOctavosFinal, $resultado);
        }

        return $this->parsearResultadosPartido(self::FASE_OCTAVOS, $resultadosOctavosFinal);
    }

    /**
     * Esta función simula los encuentros de cuartos de final en donde los clasificados
     * de los equipos se encuentran.
     * Se realiza de la siguiente manera:
     * - 1ro octavos X - 1do octavos X+1
     *
     * @param array $resultadosOctavosFinal array con los datos ordenados de los equipos clasificados
     * @param Campeonato $campeonato campeonato en juego
     * @return array array con la información de los ganadores y perdedores
     */
    public function jugarCuartosFinal(array  $resultadosOctavosFinal, Campeonato $campeonato) : array
    {
        $resultadosCuartosFinal = array();

        for ($i=0; $i < 8; $i = 2 + $i) {
            $idEquipoLocal     = $resultadosOctavosFinal[$i]["resultadoEquipos"][0]["idEquipo"];
            $equipoLocal       = $this->equipoRepository->findById($idEquipoLocal);
            $idEquipoVisitante = $resultadosOctavosFinal[$i+1]["resultadoEquipos"][0]["idEquipo"];
            $equipoVisitante   = $this->equipoRepository->findById($idEquipoVisitante);
            $resultado = $this->jugarPartido(self::FASE_CUARTOS, $equipoLocal, $equipoVisitante, $campeonato);
            array_push($resultadosCuartosFinal, $resultado);
        }

        return $this->parsearResultadosPartido(self::FASE_CUARTOS, $resultadosCuartosFinal);
    }

    /**
     * Esta función simula los encuentros de semifinal en donde los clasificados
     * de los equipos se encuentran.
     * Se realiza de la siguiente manera:
     * - 1ro cuartos X - 1do cuartos X+1
     *
     * @param array $resultadosCuartosFinal array con los datos ordenados de los equipos clasificados
     * @param Campeonato $campeonato campeonato en juego
     * @return array array con la información de los ganadores y perdedores
     */
    public function jugarSemifinal(array  $resultadosCuartosFinal, Campeonato $campeonato) : array
    {
        $resultadosSemiFinal = array();

        for ($i=0; $i < 4; $i = 2 + $i) {
            $idEquipoLocal     = $resultadosCuartosFinal[$i]["resultadoEquipos"][0]["idEquipo"];
            $equipoLocal       = $this->equipoRepository->findById($idEquipoLocal);
            $idEquipoVisitante = $resultadosCuartosFinal[$i+1]["resultadoEquipos"][0]["idEquipo"];
            $equipoVisitante   = $this->equipoRepository->findById($idEquipoVisitante);
            $resultado = $this->jugarPartido(self::FASE_SEMIFINAL, $equipoLocal, $equipoVisitante, $campeonato);
            array_push($resultadosSemiFinal, $resultado);
        }

        return $this->parsearResultadosPartido(self::FASE_SEMIFINAL, $resultadosSemiFinal);
    }

    /**
     * Esta función simula los encuentros de semifinal en donde los clasificados
     * de los equipos se encuentran.
     * Se realiza de la siguiente manera:
     * - 1ro cuartos X - 1do cuartos X+1
     *
     * @param array $resultadosCuartosFinal array con los datos ordenados de los equipos clasificados
     * @param Campeonato $campeonato campeonato en juego
     * @return array array con la información de los ganadores y perdedores
     */
    public function jugarFinal(array  $resultadosSemiFinal, Campeonato $campeonato) : array
    {
        $resultadosFinal = array();
        $i = 0;

        $idEquipoLocal     = $resultadosSemiFinal[$i]["resultadoEquipos"][0]["idEquipo"];
        $equipoLocal       = $this->equipoRepository->findById($idEquipoLocal);
        $idEquipoVisitante = $resultadosSemiFinal[$i+1]["resultadoEquipos"][0]["idEquipo"];
        $equipoVisitante   = $this->equipoRepository->findById($idEquipoVisitante);
        $resultado = $this->jugarPartido(self::FASE_FINAL, $equipoLocal, $equipoVisitante, $campeonato);
        array_push($resultadosFinal, $resultado);

        return $this->parsearResultadosPartido(self::FASE_FINAL, $resultadosFinal);
    }

    /**
     * Esta función se encarga de parsear los resultados de los partidos en las
     * fases distintas a la fase de grupos con el fin de mantener un estandar para
     * la presentacion de resultados.
     *
     * @param string $fase indica el nombre de la fase de los partidos
     * @param array $resultados con resultados de partidos a estandarizar
     * @return array con resultados formateados
     */
    public function parsearResultadosPartido(string $fase, array $resultados) : array
    {
        $resultadosParseados = array();

        $i = 0;
        foreach ($resultados as $resultado) {
            $resultadoEquipos = array();

            //Parsea equipo ganador
            $equipo = $resultado[self::CLAVE_ARRAY_GANADORES];
            $infoEquipo = array(
                "idEquipo" => $equipo->getId(),
                "nombre" => $equipo->getNombre(),
                "clasifica" => "Clasifica"
            );
            array_push($resultadoEquipos, $infoEquipo);

            //Parsea equipo perdedor
            $equipo = $resultado[self::CLAVE_ARRAY_PERDEDORES];
            $infoEquipo = array(
                "idEquipo" => $equipo->getId(),
                "nombre" => $equipo->getNombre(),
                "clasifica" => "No Clasifica"
            );
            array_push($resultadoEquipos, $infoEquipo);

            $resultadoParseado = array(
                "nombreFase" => $fase,
                "resultadoEquipos" => $resultadoEquipos
            );
            array_push($resultadosParseados, $resultadoParseado);
        }

        return $resultadosParseados;
    }

    /**
     * Esta función se encarga de simular los partidos de la fase de grupos, su
     * resultado será un array de ganadores y perdedores. Se aplica el formato
     * de todos contra todos. Los partidos son a una sola vuelta.
     *
     * @param array $grupos array que contendrá los equipos ganadores y perdedores
     * por grupo respectivamente.
     */
    public function jugarFaseDeGrupos(array $grupos, Campeonato $campeonato): array
    {
        $resultadoFaseDeGrupos = array();

        foreach ($grupos as $grupo) {
            $equiposLocales = clone $grupo->getEquipos();
            $equiposVisitantes = clone $grupo->getEquipos();
            $partidosGrupo = array();
            $i = 0;

            foreach ($equiposLocales as $equipoLocal) {
                foreach ($equiposVisitantes as $equipoVisitante) {
                    unset($equiposVisitantes[$i]);
                    if ($equipoLocal->getId() != $equipoVisitante->getId()) {
                        $resultado = $this->jugarPartido(self::FASE_GRUPOS, $equipoLocal, $equipoVisitante, $campeonato);
                        array_push($partidosGrupo, $resultado);
                    }
                }
                $i++;
            }

            $resultadoGrupo = $this->obtenerResultadosGrupo($grupo, $partidosGrupo);
            $resultadoFaseDeGrupos[$grupo->getId()] = array(
                "nombreFase" => $grupo->getNombre(),
                "resultadoEquipos" => $resultadoGrupo
            );
        }

        return $resultadoFaseDeGrupos;
    }

    /**
     * Esta funcionalidad retorna un array organizado por puntos
     * de los equipos.
     *
     * @param $grupo
     * @return array
     */
    public function obtenerResultadosGrupo(Grupo $grupo, array $partidosGrupo) : array
    {
        $equipos = array();

        //Se establece un puntaje de cero para cada equipo
        foreach ($grupo->getEquipos() as $equipo) {
            $equipos[$equipo->getId()] = 0;
        }

        //Se itera sobre los equipos ganadores para dar un puntaje y clasificar el grupo
        foreach ($partidosGrupo as $partido) {
            $equipoGanador = $partido[self::CLAVE_ARRAY_GANADORES];
            $pos = $equipos[$equipoGanador->getId()];
            $pos++;
            $equipos[$equipoGanador->getId()] = $pos;
        }

        //Se ordena de mayor a menor por valor
        arsort($equipos);

        //Teniendo organizado el arreglo se ponen nombres para devolver la información legible
        $equiposOrganizados = array();
        $limClasifican = 2;
        $itClasifican = 0;
        foreach ($equipos as $key => $equipoArray) {
            $clasifica = "No clasifica";
            if ($itClasifican < $limClasifican) {
                $clasifica = "Clasifica";
            }
            foreach ($grupo->getEquipos() as $equipo) {
                if($key == $equipo->getId()) {
                    $partidosGanados = $equipos[$equipo->getId()];
                    $infoEquipo = array(
                        "idEquipo" => $equipo->getId(),
                        "nombre" => $equipo->getNombre(),
                        "partidosGanados" => $partidosGanados,
                        "clasifica" => $clasifica
                    );
                }
            }
            array_push($equiposOrganizados, $infoEquipo);
            $itClasifican++;
        }

        return $equiposOrganizados;
    }

    /**
     * Esta función realiza la simulación de un partido. Creará instancias aleatorias
     * de tarjetas rojas, tarjetas amarrillas y también instancias de goles.
     *
     * @param string $fase
     * @param Equipo $equipoLocal
     * @param Equipo $equipoVisitante
     * @param Campeonato $campeonato
     */
    public function jugarPartido(string $fase, Equipo $equipoLocal, Equipo $equipoVisitante, Campeonato $campeonato): array
    {
        //Se crea el partido
        $partido = $this->partidoRepository->save(new \DateTime(), $fase, $equipoLocal, $equipoVisitante, $campeonato);

        //Se crea el resultado partido para adicionarle los goles y tarjetas
        $resultadoPartidoPuntos    = 0;
        $resultadoPartidoEsGanador = 0;
        $resultadoPartidoLocal     = $this->resultadoPartidoRepository->save($resultadoPartidoPuntos, $resultadoPartidoEsGanador, $partido, $equipoLocal);
        $resultadoPartidoVisitante = $this->resultadoPartidoRepository->save($resultadoPartidoPuntos, $resultadoPartidoEsGanador, $partido, $equipoVisitante);

        //Se simulan los resultados
        $totalGolesLocal      = $this->simularGoles($equipoLocal, $resultadoPartidoLocal);
        $totalGolesVisitante  = $this->simularGoles($equipoVisitante, $resultadoPartidoVisitante);
        $totalFaltasLocal     = $this->simularTarjetas($equipoLocal, $resultadoPartidoLocal);
        $totalFaltasVisitante = $this->simularTarjetas($equipoVisitante, $resultadoPartidoVisitante);

        //Se revisa si hay empate
        if ($totalGolesLocal == $totalGolesVisitante) {
            $equipoGanador = $this->desempatarPartido($equipoLocal, $equipoVisitante, $campeonato);
            if ($equipoGanador->getId() == $equipoLocal->getId()) {
                $equipoGanador  = $equipoLocal;
                $equipoPerdedor = $equipoVisitante;
            } else {
                $equipoGanador = $equipoVisitante;
                $equipoPerdedor = $equipoLocal;
            }
        } else if ($totalGolesLocal > $totalGolesVisitante) {
            $equipoGanador  = $equipoLocal;
            $equipoPerdedor = $equipoVisitante;
        } else {
            $equipoGanador  = $equipoVisitante;
            $equipoPerdedor = $equipoLocal;
        }

        //Se actualiza el resultado del partido para definir el ganador
        $resultadoPartidoPuntos = 3;
        $resultadoPartidoEsGanador = 1;
        if ($equipoGanador->getId() == $equipoLocal->getId()) {
            $resultadoPartidoLocal = $this->resultadoPartidoRepository->updateResultado($resultadoPartidoLocal->getId(), $resultadoPartidoPuntos, $resultadoPartidoEsGanador);
        } else {
            $resultadoPartidoVisitante = $this->resultadoPartidoRepository->updateResultado($resultadoPartidoVisitante->getId(), $resultadoPartidoPuntos, $resultadoPartidoEsGanador);
        }

        return array(
            self::CLAVE_ARRAY_GANADORES  => $equipoGanador,
            self::CLAVE_ARRAY_PERDEDORES => $equipoPerdedor
        );
    }

    /**
     * Esta función se encarga de simular goles del minuto 1 al 90.
     * Seleccionará un jugador al azar y asimismo hará una cantidad
     * de iteraciones entre 1 a 3.
     *
     * @param Equipo $equipo
     * @param ResultadoPartido $resultadoPartido
     */
    public function simularGoles(Equipo $equipo, ResultadoPartido $resultadoPartido): int
    {
        $cantidadGoles = 0;
        $iteraciones = rand(1, 3);
        for ($i = 0; $i < $iteraciones; $i++) {
            $minuto = rand(1, 90);
            $minuto = strval($minuto);
            $cantJugadores = $equipo->getJugadores()->count() - 1;
            $posJugador = rand(0, $cantJugadores);
            $jugador = $equipo->getJugadores()->get($posJugador);

            $this->golRepository->save($minuto, $jugador, $resultadoPartido);
            $cantidadGoles++;
        }

        return $cantidadGoles;
    }

    /**
     * Esta función se encarga de simular faltas del minuto 1 al 90.
     * Seleccionará un jugador al azar y asimismo hará una cantidad
     * de iteraciones entre 1 a 3.
     *
     * @param Equipo $equipo
     * @param ResultadoPartido $resultadoPartido
     */
    public function simularTarjetas(Equipo $equipo, ResultadoPartido $resultadoPartido): int
    {
        $iteraciones = rand(1, 3);
        $cantidadTarjetas = 0;
        for ($i = 0; $i < $iteraciones; $i++) {
            $minuto = rand(1, 90);
            $minuto = strval($minuto);
            $cantJugadores = $equipo->getJugadores()->count() - 1;
            $posJugador = rand(0, $cantJugadores);
            $jugador = $equipo->getJugadores()->get($posJugador);

            $colorRandom = rand(0, 1);
            $color = self::TARJETA_AMARRILLA;
            if ($colorRandom) {
                $color = self::TARJETA_ROJA;
            }

            $this->tarjetaRepository->save($color, $minuto, $jugador, $resultadoPartido);
            $cantidadTarjetas++;
        }

        return $cantidadTarjetas;
    }

    /**
     * Esta función se encarga de realizar el desempate en un encuentro, teniendo en cuenta
     * que gana el equipo que:
     * - Tenga menos partidos perdidos.
     * - Tenga más goles en lo que va del campeonato.
     * - Tenga menos tarjetas rojas acumuladas.
     * - Tenga menos tarjetas amarillas acumuladas.
     *
     * @param Equipo $equipoLocal
     * @param Equipo $equipoVisitante
     */
    public function desempatarPartido(Equipo $equipoLocal, Equipo $equipoVisitante, Campeonato $campeonato)
    {
        $puntosLocal = 0;
        $puntosVisitante = 0;

        $ganadosAcumuladosLocal = $this->equipoRepository->getCantidadPartidosGanados($equipoLocal->getId(), $campeonato->getId());
        $ganadosAcumuladosVisitante = $this->equipoRepository->getCantidadPartidosGanados($equipoVisitante->getId(), $campeonato->getId());

        $golesAcumuladosLocal = $this->equipoRepository->getCantidadGolesAcumulados($equipoLocal->getId(), $campeonato->getId());
        $golesAcumuladosVisitante = $this->equipoRepository->getCantidadGolesAcumulados($equipoVisitante->getId(), $campeonato->getId());

        $tarjetasRojasAcumuladosLocal = $this->equipoRepository->getCantidadTarjetasAcumuladas(self::TARJETA_ROJA, $equipoLocal->getId(), $campeonato->getId());
        $tarjetasRojasAcumuladosVisitante = $this->equipoRepository->getCantidadTarjetasAcumuladas(self::TARJETA_ROJA, $equipoVisitante->getId(), $campeonato->getId());

        $tarjetasAmarillasAcumuladosLocal = $this->equipoRepository->getCantidadTarjetasAcumuladas(self::TARJETA_AMARRILLA, $equipoLocal->getId(), $campeonato->getId());
        $tarjetasAmarillasAcumuladosVisitante = $this->equipoRepository->getCantidadTarjetasAcumuladas(self::TARJETA_AMARRILLA, $equipoVisitante->getId(), $campeonato->getId());

        if ($ganadosAcumuladosLocal > $ganadosAcumuladosVisitante) {
            $puntosLocal++;
        } else {
            $puntosVisitante++;
        }

        if ($golesAcumuladosLocal > $golesAcumuladosVisitante) {
            $puntosLocal++;
        } else {
            $puntosVisitante++;
        }

        if ($tarjetasRojasAcumuladosLocal < $tarjetasRojasAcumuladosVisitante) {
            $puntosLocal++;
        } else {
            $puntosVisitante++;
        }

        if ($tarjetasAmarillasAcumuladosLocal < $tarjetasAmarillasAcumuladosVisitante) {
            $puntosLocal++;
        } else {
            $puntosVisitante++;
        }

        if ($puntosLocal > $puntosVisitante) {
            return $equipoLocal;
        } else {
            return $equipoVisitante;
        }

    }
}