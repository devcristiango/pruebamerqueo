<?php

namespace App\UseCases\Campeonato;

use App\Repository\Campeonato\ICampeonatoRepository;

class ObtenerTablaPosicionesUseCase
{
    /** @var ICampeonatoRepository $campeonatoRepository */
    private $campeonatoRepository;

    public function __construct(ICampeonatoRepository $campeonatoRepository)
    {
        $this->campeonatoRepository = $campeonatoRepository;
    }

    /**
     * Esta función consulta en base de datos y devuelve un arreglo con la información
     * de la tabla de posiciones.
     *
     * @param int $campeonatoId identificador del campeonato si == 0 entonces usa last id
     * de campeonato.
     * @return array|null array con la información de la tabla de posiciones
     */
    public function __invoke(int $campeonatoId) : ?array
    {
        $informacionTablaPosiciones = array();

        if ($campeonatoId == 0) {
            $campeonatoId = $this->campeonatoRepository->getLastIdCampeonato();
        }

        $informacionTotales = $this->campeonatoRepository->getResultadosTotalesPartidos($campeonatoId);
        $informacionTablaPosiciones["posiciones"] = $informacionTotales;
        $informacionTotales = $this->campeonatoRepository->getTarjetasTotalesPartidos($campeonatoId);
        $informacionTablaPosiciones["tarjetas"] = $informacionTotales;
        $informacionTotales = $this->campeonatoRepository->getGolesTotalesPartidos($campeonatoId);
        $informacionTablaPosiciones["goles"] = $informacionTotales;
        $informacionTotales = $this->campeonatoRepository->getPartidosJugados($campeonatoId);
        $informacionTablaPosiciones["partidos"] = $informacionTotales;

        return $informacionTablaPosiciones;
    }
}