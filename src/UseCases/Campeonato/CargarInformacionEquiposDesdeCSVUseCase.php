<?php

namespace App\UseCases\Campeonato;

use App\Repository\Campeonato\ICampeonatoRepository;
use App\Repository\Equipo\IEquipoRepository;
use App\Repository\Grupo\IGrupoRepository;
use App\Repository\Jugador\IJugadorRepository;
use App\Services\Helpers\LectorCSV\ILectorCSV;

class CargarInformacionEquiposDesdeCSVUseCase
{
    /** @var ICampeonatoRepository */
    private $campeonatoRepository;

    /** @var IGrupoRepository  */
    private $grupoRepository;

    /** @var IEquipoRepository  */
    private $equipoRepository;

    /** @var IJugadorRepository */
    private $jugadorRepository;

    public function __construct(
        ICampeonatoRepository $campeonatoRepository,
        IGrupoRepository $grupoRepository,
        IEquipoRepository $equipoRepository,
        IJugadorRepository $jugadorRepository
    ) {
        $this->campeonatoRepository = $campeonatoRepository;
        $this->grupoRepository = $grupoRepository;
        $this->equipoRepository = $equipoRepository;
        $this->jugadorRepository = $jugadorRepository;
    }

    /**
     * Esta función realiza la lectura del archivo csv cuyo path se envíe por parámetro
     * o caso contrario realizará la lectura del path por defecto.
     *
     * @param ILectorCSV $lector
     * @param string $path
     * @return bool
     */
    public function __invoke(ILectorCSV $lector, string $path): bool
    {
        try {
            $informacionEquipos = $lector->leerCSV($path);

            foreach ($informacionEquipos as $informacionEquipo) {
                list(
                    $grupoId,
                    $grupoNombre,
                    $equipoId,
                    $equipoNombre,
                    $equipoBandera,
                    $jugadorId,
                    $jugadorNombre,
                    $jugadorNacionalidad,
                    $jugadorEdad,
                    $jugadorPosicion,
                    $jugadorNumeroCamiseta,
                    $jugadorFoto
                    ) = $informacionEquipo;

                $grupo   = $this->grupoRepository->save((int) $grupoId, $grupoNombre);

                $equipo  = $this->equipoRepository->save((int) $equipoId, $equipoNombre, $equipoBandera, $grupo);

                $jugador = $this->jugadorRepository->save(
                    (int) $jugadorId,
                    $jugadorNombre,
                    $jugadorNacionalidad,
                    $jugadorEdad,
                    $jugadorPosicion,
                    $jugadorNumeroCamiseta,
                    $jugadorFoto,
                    $equipo
                );
            }
        } catch (\Throwable $th) {
            return false;
        }

        return true;
    }
}