<?php

namespace App\Repository\Gol;

use App\Entity\Gol;
use App\Entity\Jugador;
use App\Entity\ResultadoPartido;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Gol|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gol|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gol[]    findAll()
 * @method Gol[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GolDoctrineRepository extends ServiceEntityRepository implements IGolRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gol::class);
    }

    /**
     * Crea y almacena una instancia de Gol en la base de datos
     *
     * @param string $golMinuto
     * @param Jugador $jugador
     * @param ResultadoPartido $resultadoPartido
     * @return Gol
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function  save(string $golMinuto, Jugador $jugador, ResultadoPartido $resultadoPartido): Gol
    {
        $gol = new Gol();
        $gol->setMinuto($golMinuto);
        $gol->setJugador($jugador);
        $gol->setResultadoPartido($resultadoPartido);

        $this->getEntityManager()->persist($gol);
        $this->getEntityManager()->flush();

        return $gol;
    }
}
