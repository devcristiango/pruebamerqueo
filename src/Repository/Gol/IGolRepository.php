<?php

namespace App\Repository\Gol;

use App\Entity\Gol;
use App\Entity\Jugador;
use App\Entity\ResultadoPartido;

interface IGolRepository
{
    public function save(string $golMinuto, Jugador $jugador, ResultadoPartido $resultadoPartido) : Gol;
}