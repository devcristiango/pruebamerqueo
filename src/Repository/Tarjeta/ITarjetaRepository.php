<?php

namespace App\Repository\Tarjeta;

use App\Entity\Jugador;
use App\Entity\ResultadoPartido;
use App\Entity\Tarjeta;

interface ITarjetaRepository
{
    public function save(string  $tarjetaColor, string $tarjetaMinuto, Jugador $jugador, ResultadoPartido $resultadoPartido) : Tarjeta;
}