<?php

namespace App\Repository\Tarjeta;

use App\Entity\Jugador;
use App\Entity\ResultadoPartido;
use App\Entity\Tarjeta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tarjeta|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tarjeta|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tarjeta[]    findAll()
 * @method Tarjeta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TarjetaDoctrineRepository extends ServiceEntityRepository implements ITarjetaRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tarjeta::class);
    }

    /**
     * Crea y almacena en base de datos la instancia Tarjeta
     *
     * @param string $tarjetaColor
     * @param string $tarjetaMinuto
     * @param Jugador $jugador
     * @param ResultadoPartido $resultadoPartido
     * @return Tarjeta
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(string  $tarjetaColor, string $tarjetaMinuto, Jugador $jugador, ResultadoPartido $resultadoPartido) : Tarjeta
    {
        $tarjeta = new Tarjeta();
        $tarjeta->setColor($tarjetaColor);
        $tarjeta->setMinuto($tarjetaMinuto);
        $tarjeta->setJugador($jugador);
        $tarjeta->setResultadoPartido($resultadoPartido);

        $this->getEntityManager()->persist($tarjeta);
        $this->getEntityManager()->flush();

        return $tarjeta;
    }
}
