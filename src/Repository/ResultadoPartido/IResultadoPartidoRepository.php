<?php

namespace App\Repository\ResultadoPartido;

use App\Entity\Equipo;
use App\Entity\Partido;
use App\Entity\ResultadoPartido;

interface IResultadoPartidoRepository
{
    public function save(int $resultadoPartidoPuntos, int $resultadoPartidoEsGanador, Partido $partido, Equipo $equipo) : ResultadoPartido;
    public function updateResultado(int $resultadoPartidoId, int $resultadoPartidoPuntos, int $resultadoPartidoEsGanador);
}