<?php

namespace App\Repository\ResultadoPartido;

use App\Entity\Equipo;
use App\Entity\Partido;
use App\Entity\ResultadoPartido;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ResultadoPartido|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResultadoPartido|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResultadoPartido[]    findAll()
 * @method ResultadoPartido[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResultadoPartidoDoctrineRepository extends ServiceEntityRepository implements IResultadoPartidoRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResultadoPartido::class);
    }


    /**
     * Crea y registra una nueva entidad de ResultadoPartido.
     *
     * @param int $resultadoPartidoPuntos
     * @param int $resultadoPartidoEsGanador
     * @param Partido $partido
     * @param Equipo $equipo
     * @return ResultadoPartido
     */
    public function save(int $resultadoPartidoPuntos, int $resultadoPartidoEsGanador, Partido $partido, Equipo $equipo): ResultadoPartido
    {
        $resultadoPartido = new ResultadoPartido();
        $resultadoPartido->setPuntos($resultadoPartidoPuntos);
        $resultadoPartido->setEsGanador($resultadoPartidoEsGanador);
        $resultadoPartido->setPartido($partido);
        $resultadoPartido->setEquipo($equipo);

        $this->getEntityManager()->persist($resultadoPartido);
        $this->getEntityManager()->flush();

        //Se actualizan las referencias
        $partido->addResultadoPartido($resultadoPartido);
        $equipo->addResultadoPartido($resultadoPartido);
        $this->getEntityManager()->persist($partido);
        $this->getEntityManager()->persist($equipo);
        $this->getEntityManager()->flush();

        return $resultadoPartido;
    }

    /**
     * Actualiza la información del ResultadoPartido identificado
     * por id.
     *
     * @param int $resultadoPartidoId
     * @param int $resultadoPartidoPuntos
     * @param int $resultadoPartidoEsGanador
     * @return mixed
     */
    public function updateResultado(int $resultadoPartidoId, int $resultadoPartidoPuntos, int $resultadoPartidoEsGanador)
    {
        $resultadoPartido = $this->getEntityManager()->find(ResultadoPartido::class, $resultadoPartidoId);
        $resultadoPartido->setPuntos($resultadoPartidoPuntos);
        $resultadoPartido->setEsGanador($resultadoPartidoEsGanador);
        $this->getEntityManager()->persist($resultadoPartido);
        $this->getEntityManager()->flush();
    }
}
