<?php

namespace App\Repository\Equipo;

use App\Entity\Campeonato;
use App\Entity\Equipo;
use App\Entity\Grupo;

interface IEquipoRepository
{
    public function save(int $equipoId, string $equipoNombre, string $equipoBandera, Grupo $grupo) : Equipo;

    public function findById(int $equipoId) : ?Equipo;

    public function getCantidadPartidosGanados(int $equipoId, int $campeonatoId) : int;

    public function getCantidadGolesAcumulados(int $equipoId, int $campeonatoId) : int;

    public function getCantidadTarjetasAcumuladas(string $tarjetaColor, int $equipoId, int $campeonatoId) : int;
}