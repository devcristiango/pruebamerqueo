<?php

namespace App\Repository\Equipo;

use App\Entity\Equipo;
use App\Entity\Grupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * @method Equipo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Equipo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Equipo[]    findAll()
 * @method Equipo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipoDoctrineRepository extends ServiceEntityRepository implements IEquipoRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Equipo::class);
    }


    /**
     * Crea y almacena en base de datos el equipo
     *
     * @param int $equipoId
     * @param string $equipoNombre
     * @param string $equipoBandera
     * @param Grupo $grupo
     * @return Equipo
     */
    public function save(int $equipoId, string $equipoNombre, string $equipoBandera, Grupo $grupo): Equipo
    {
        $equipo = $this->find($equipoId);

        if (!isset($equipo)) {
            $equipo = new Equipo();
            $equipo->setId($equipoId);
            $equipo->setNombre($equipoNombre);
            $equipo->setBandera($equipoBandera);
            $this->getEntityManager()->persist($equipo);
            $this->getEntityManager()->flush();
        }

        //Se actualiza la relación de equipos con grupo
        $equipo->setGrupo($grupo);
        $grupo->removeEquipo($equipo);
        $grupo->addEquipo($equipo);
        $this->getEntityManager()->persist($equipo);
        $this->getEntityManager()->persist($grupo);
        $this->getEntityManager()->flush();

        return $equipo;
    }

    /**
     * Retorna el equipo identificado por id
     *
     * @param int $equipoId
     * @return Equipo
     */
    public function findById(int $equipoId): ?Equipo
    {
        return $this->find($equipoId);
    }

    /**
     * Obtiene la cantidad de partidos ganados acumulados del equipo identificado
     * por id en el campeonato dado por id
     *
     * @param int $equipoId
     * @param int $campeonatoId
     * @return int cantidad de partidos ganados
     */
    public function getCantidadPartidosGanados(int $equipoId, int $campeonatoId): int
    {
        $sqlNative = "SELECT 
                        count(*) as partidosGanados
                    FROM 
                        equipo e
                    INNER JOIN 
                        resultado_partido rp ON rp.equipo_id = e.id
                    INNER JOIN
                        partido p ON p.id = rp.partido_id
                    INNER JOIN 
                        campeonato c ON p.campeonato_id = $campeonatoId
                    WHERE
                        e.id = $equipoId AND 
                        rp.es_ganador = 1
                    ";
        $con = $this->getEntityManager()->getConnection();
        $st = $con->executeQuery($sqlNative);

        //recuperamos las tuplas de resultados
        $rs = $st->fetchAll();

        return $rs[0]["partidosGanados"];
    }

    /**
     * Obtiene la cantidad de goles acumulados del equipo identificado por id
     * en el campeonato dado por id
     *
     * @param int $equipoId
     * @param int $campeonatoId
     * @return int cantidad de goles acumulados
     */
    public function getCantidadGolesAcumulados(int $equipoId, int $campeonatoId): int
    {
        $sqlNative = "SELECT 
                        count(*) as golesRealizados
                    FROM 
                        equipo e
                    INNER JOIN 
                        resultado_partido rp ON rp.equipo_id = e.id
                    INNER JOIN
                        partido p ON p.id = rp.partido_id
                    INNER JOIN 
                        campeonato c ON p.campeonato_id = $campeonatoId
                    INNER JOIN
                        gol g ON g.resultado_partido_id = rp.id
                    WHERE
                        e.id = $equipoId
                    ";
        $con = $this->getEntityManager()->getConnection();
        $st = $con->executeQuery($sqlNative);

        //recuperamos las tuplas de resultados
        $rs = $st->fetchAll();

        return $rs[0]["golesRealizados"];
    }

    /**
     * Obtiene la cantidad de tarjetas acumuladas
     *
     * @param string $tarjetaColor
     * @param int $equipoId
     * @param int $campeonatoId
     * @return int
     */
    public function getCantidadTarjetasAcumuladas(string $tarjetaColor, int $equipoId, int $campeonatoId): int
    {
        $sqlNative = "SELECT 
                        count(*) as cantidadTarjetas
                    FROM 
                        equipo e
                    INNER JOIN 
                        resultado_partido rp ON rp.equipo_id = e.id
                    INNER JOIN
                        partido p ON p.id = rp.partido_id
                    INNER JOIN 
                        campeonato c ON p.campeonato_id = $campeonatoId
                    INNER JOIN
                        tarjeta t ON t.resultado_partido_id = rp.id
                    WHERE
                        e.id = $equipoId AND t.color = '$tarjetaColor'
                     ";
        $con = $this->getEntityManager()->getConnection();
        $st = $con->executeQuery($sqlNative);

        //recuperamos las tuplas de resultados
        $rs = $st->fetchAll();

        return $rs[0]["cantidadTarjetas"];
    }
}
