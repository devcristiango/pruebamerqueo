<?php

namespace App\Repository\Jugador;

use App\Entity\Equipo;
use App\Entity\Jugador;

interface IJugadorRepository
{
    public function save(
        int $jugadorId,
        string $jugadorNombre,
        string $jugadorNacionalidad,
        string $jugadorEdad,
        string $jugadorPosicion,
        string $jugadorNumeroCamiseta,
        string $jugadorFoto,
        Equipo $equipo
    ) : Jugador;


}