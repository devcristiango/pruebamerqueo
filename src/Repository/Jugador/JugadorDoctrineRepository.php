<?php

namespace App\Repository\Jugador;

use App\Entity\Equipo;
use App\Entity\Jugador;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Jugador|null find($id, $lockMode = null, $lockVersion = null)
 * @method Jugador|null findOneBy(array $criteria, array $orderBy = null)
 * @method Jugador[]    findAll()
 * @method Jugador[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JugadorDoctrineRepository extends ServiceEntityRepository implements IJugadorRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Jugador::class);
    }

    /**
     * Esta función crea y almacena una instancia de Jugador en base de datos
     *
     * @param int $jugadorId
     * @param string $jugadorNombre
     * @param string $jugadorNacionalidad
     * @param string $jugadorEdad
     * @param string $jugadorPosicion
     * @param string $jugadorNumeroCamiseta
     * @param string $jugadorFoto
     * @param Equipo $equipo
     * @return Jugador
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(
        int $jugadorId,
        string $jugadorNombre,
        string $jugadorNacionalidad,
        string $jugadorEdad,
        string $jugadorPosicion,
        string $jugadorNumeroCamiseta,
        string $jugadorFoto,
        Equipo $equipo
    ) : Jugador
    {
        $jugador = $this->find($jugadorId);

        if (!isset($jugador)) {
            $jugador = new Jugador();
            $jugador->setId($jugadorId);
            $jugador->setNombre($jugadorNombre);
            $jugador->setNacionalidad($jugadorNacionalidad);
            $jugador->setEdad($jugadorEdad);
            $jugador->setPosicion($jugadorPosicion);
            $jugador->setNumeroCamiseta($jugadorNumeroCamiseta);
            $jugador->setFoto($jugadorFoto);
            $this->getEntityManager()->persist($jugador);
            $this->getEntityManager()->flush();
        }

        //Se actualiza la relación con equipo
        $jugador->setEquipo($equipo);
        $equipo->removeJugador($jugador);
        $equipo->addJugador($jugador);
        $this->getEntityManager()->persist($jugador);
        $this->getEntityManager()->persist($equipo);
        $this->getEntityManager()->flush();

        return $jugador;
    }
}
