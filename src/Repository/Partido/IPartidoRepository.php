<?php

namespace App\Repository\Partido;

use App\Entity\Campeonato;
use App\Entity\Equipo;
use App\Entity\Partido;

interface IPartidoRepository
{
    public function save(\DateTimeInterface $fecha_hora, string $fase, Equipo $equipoLocal, Equipo $equipoVisitante, Campeonato $campeonato) : Partido;
}