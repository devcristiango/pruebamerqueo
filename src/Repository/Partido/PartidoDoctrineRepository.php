<?php

namespace App\Repository\Partido;

use App\Entity\Campeonato;
use App\Entity\Equipo;
use App\Entity\Partido;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Partido|null find($id, $lockMode = null, $lockVersion = null)
 * @method Partido|null findOneBy(array $criteria, array $orderBy = null)
 * @method Partido[]    findAll()
 * @method Partido[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartidoDoctrineRepository extends ServiceEntityRepository implements IPartidoRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Partido::class);
    }


    /**
     * Esta función crea y almacena en base de datos una instancia de Partido
     *
     * @param \DateTimeInterface $fecha_hora
     * @param string $fase
     * @param Equipo $equipoLocal
     * @param Equipo $equipoVisitante
     * @param Campeonato $campeonato
     * @return Partido
     */
    public function save(\DateTimeInterface $fecha_hora, string $fase, Equipo $equipoLocal, Equipo $equipoVisitante, Campeonato $campeonato): Partido
    {
        $partido = new Partido();
        $partido->setFechaHora($fecha_hora);
        $partido->setFase($fase);
        $partido->setEquipoLocal($equipoLocal);
        $partido->setEquipoVisitante($equipoVisitante);
        $partido->setCampeonato($campeonato);

        $this->getEntityManager()->persist($partido);
        $this->getEntityManager()->flush();

        //Se actualizan los partidos del campeonato
        $campeonato->addPartido($partido);
        $this->getEntityManager()->persist($campeonato);
        $this->getEntityManager()->flush();

        return $partido;
    }
}
