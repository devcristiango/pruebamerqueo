<?php

namespace App\Repository\Campeonato;

use App\Entity\Campeonato;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Campeonato|null find($id, $lockMode = null, $lockVersion = null)
 * @method Campeonato|null findOneBy(array $criteria, array $orderBy = null)
 * @method Campeonato[]    findAll()
 * @method Campeonato[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampeonatoDoctrineRepository extends ServiceEntityRepository implements ICampeonatoRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Campeonato::class);
    }

    /**
     * Crea y almacena en base de datos una instancia de Campeonato
     *
     * @param string $campeonatoNombre
     * @return Campeonato
     */
    public function save(string $campeonatoNombre): Campeonato
    {
        $campeonato = new Campeonato();
        $campeonato->setNombre($campeonatoNombre);

        $this->getEntityManager()->persist($campeonato);
        $this->getEntityManager()->flush();

        return $campeonato;
    }


    /**
     * Obtiene los totales de partidos jugados y ganados
     * por los equipos en el campeonato identificado por id
     *
     * @param int $campeonatoId
     * @return array
     */
    public function getResultadosTotalesPartidos(int $campeonatoId): array
    {
        $sqlNative = "SELECT 
                        gr.nombre as grupoNombre,
                        e.nombre as equipoNombre,
                        count(rp.id) as partidosJugados,
                        sum(rp.es_ganador) as partidosGanados,
                        count(rp.id) - sum(rp.es_ganador) as partidosPerdidos
                    FROM 
                        campeonato c
                    INNER JOIN 
                        partido p on
                        c.id = p.campeonato_id
                    INNER JOIN
                        resultado_partido rp on
                        rp.partido_id = p.id
                    INNER JOIN 
                        equipo e on
                        rp.equipo_id = e.id
                    INNER JOIN
                        grupo gr on
                        gr.id = e.grupo_id
                    WHERE
                        c.id = $campeonatoId
                    GROUP BY
                        e.id
                    ORDER BY
                        partidosGanados DESC";
        $con = $this->getEntityManager()->getConnection();
        $st = $con->executeQuery($sqlNative);

        //recuperamos las tuplas de resultados
        $rs = $st->fetchAll();

        return $rs;
    }

    /**
     * Obtiene los totales de goles para partidos
     * jugados por los equipos en el campeonato identificado por id
     *
     * @param int $campeonatoId
     * @return array
     */
    public function getGolesTotalesPartidos(int $campeonatoId): array
    {
        $sqlNative = "SELECT 
                            e.nombre as equipoNombre,
                            count(g.id) as totalGoles
                        FROM 
                            gol g 
                        INNER JOIN 
                            resultado_partido rp ON 
                            rp.id = g.resultado_partido_id 
                        INNER JOIN 
                            jugador j on
                            j.id = g.jugador_id 
                        INNER JOIN 
                            equipo e on
                            j.equipo_id = e.id 
                        WHERE 
                            rp.partido_id in (
                                SELECT id FROM partido p WHERE p.campeonato_id  = $campeonatoId
                            )
                        GROUP BY
                            e.id 
                        ORDER BY
                            totalGoles DESC";



        $con = $this->getEntityManager()->getConnection();
        $st = $con->executeQuery($sqlNative);

        //recuperamos las tuplas de resultados
        $rs = $st->fetchAll();

        return $rs;
    }

    /**
     * Obtiene los totales de tarjetas para los partidos
     * jugados en el campeonato identificado por id
     *
     * @param int $campeonatoId
     * @return array
     */
    public function getTarjetasTotalesPartidos(int $campeonatoId): array
    {
        $sqlNative = "SELECT
                            e.nombre as nombreEquipo,
                            count(t.id) as totalTarjetas,
                            t.color as colorTarjeta
                        FROM
                            tarjeta t
                        INNER JOIN resultado_partido rp on
                            rp.id = t.resultado_partido_id 
                        INNER JOIN jugador j on
                            j.id = t.jugador_id
                        INNER JOIN equipo e on
                            j.equipo_id = e.id
                        WHERE 
                            rp.partido_id in (
                                SELECT id FROM partido p WHERE p.campeonato_id  = $campeonatoId
                            )
                        GROUP BY
                            t.color,
                            e.id
                        ORDER BY
                            e.id";
        $con = $this->getEntityManager()->getConnection();
        $st = $con->executeQuery($sqlNative);

        //recuperamos las tuplas de resultados
        $rs = $st->fetchAll();

        return $rs;
    }

    /**
     * Retorna el id del ultimo campeonato
     *
     * @return int
     */
    public function getLastIdCampeonato(): int
    {
        $sqlNative = "SELECT 
                        MAX(id) as lastId
                    FROM 
                        campeonato
                    ";
        $con = $this->getEntityManager()->getConnection();
        $st = $con->executeQuery($sqlNative);

        //recuperamos las tuplas de resultados
        $rs = $st->fetchAll();

        return $rs[0]["lastId"];
    }

    /**
     * Este método retorna los encuentros que se llevaron a cabo en
     * orden ascedente.
     *
     * @param int $campeonatoId
     * @return int
     */
    public function getPartidosJugados(int $campeonatoId): array
    {
        $sqlNative = "SELECT 
                        p.fase as fase,
                        p.fecha_hora as fechaHora,
                        el.nombre as EquipoLocal,
                        (select count(*) from gol g
                        inner join resultado_partido rp on g.resultado_partido_id = rp.id 
                        where rp.equipo_id = el.id and rp.partido_id = p.id) as golesLocal,
                        ev.nombre as EquipoVisitante,
                        (select count(*) from gol g
                        inner join resultado_partido rp on g.resultado_partido_id = rp.id 
                        where rp.equipo_id = ev.id and rp.partido_id = p.id) as golesVisitante,
                        (select count(*) from tarjeta t
                        inner join resultado_partido rp on t.resultado_partido_id = rp.id 
                        where rp.equipo_id = el.id and rp.partido_id = p.id and t.color = 'amarilla') as tarjetasAmarillasLocal,
                        (select count(*) from tarjeta t
                        inner join resultado_partido rp on t.resultado_partido_id = rp.id 
                        where rp.equipo_id = el.id and rp.partido_id = p.id and t.color = 'roja') as tarjetasRojasLocal,
                        (select count(*) from tarjeta t
                        inner join resultado_partido rp on t.resultado_partido_id = rp.id 
                        where rp.equipo_id = ev.id and rp.partido_id = p.id and t.color = 'amarilla') as tarjetasAmarillasVisitante,
                        (select count(*) from tarjeta t
                        inner join resultado_partido rp on t.resultado_partido_id = rp.id 
                        where rp.equipo_id = ev.id and rp.partido_id = p.id and t.color = 'roja') as tarjetasRojasVisitante
                    FROM 
                        partido p 
                    INNER JOIN
                        equipo el on el.id = p.equipo_local_id 
                    INNER JOIN 
                        equipo ev on ev.id = p.equipo_visitante_id 
                    WHERE 
                        p.campeonato_id = $campeonatoId
                    ORDER BY 
                        p.id ASC 
                    ";
        $con = $this->getEntityManager()->getConnection();
        $st = $con->executeQuery($sqlNative);

        //recuperamos las tuplas de resultados
        $rs = $st->fetchAll();

        return $rs;
    }
}
