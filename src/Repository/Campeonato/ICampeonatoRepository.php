<?php

namespace App\Repository\Campeonato;

use App\Entity\Campeonato;

interface ICampeonatoRepository
{
    public function save(string $campeonatoNombre) : Campeonato;

    public function getResultadosTotalesPartidos(int $campeonatoId) : array;

    public function getGolesTotalesPartidos(int $campeonatoId) : array;

    public function getTarjetasTotalesPartidos(int $campeonatoId) : array;

    public function getLastIdCampeonato() : int;

    public function getPartidosJugados(int $campeonatoId) : array;
}