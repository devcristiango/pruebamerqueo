<?php

namespace App\Repository\Grupo;

use App\Entity\Grupo;
use App\Kernel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Grupo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Grupo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Grupo[]    findAll()
 * @method Grupo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GrupoDoctrineRepository extends ServiceEntityRepository implements IGrupoRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Grupo::class);
    }

    /**
     * Crea y almacena una instancia de Grupo en base de datos
     *
     * @param int $id
     * @return Grupo
     */
    public function save(int $grupoId, string $grupoNombre): Grupo
    {
        $grupo = $this->find($grupoId);

        if (!isset($grupo)) {
            $grupo = new Grupo();
            $grupo->setId($grupoId);
            $grupo->setNombre($grupoNombre);
            $this->getEntityManager()->persist($grupo);
            $this->getEntityManager()->flush();
        }

        return $grupo;
    }

    /**
     * @return array
     */
    public function findAllGrupos(): array
    {
        return $this->createQueryBuilder('g')
            ->orderBy('g.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
}
