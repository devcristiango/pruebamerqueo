<?php

namespace App\Repository\Grupo;

use App\Entity\Grupo;

interface IGrupoRepository
{
    public function save(int $grupoId, string $grupoNombre) : Grupo;

    public function findAllGrupos() : array;
}