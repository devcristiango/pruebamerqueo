<?php

namespace App\Controller\Grupo;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\UseCases\Grupo\ListarInformacionEquiposInscritosUseCase;
use App\Repository\Grupo\GrupoDoctrineRepository;

class GrupoGetController extends AbstractController
{
    /**
     * Esta acción se encarga de listar los equipos
     * agrupados por su grupo respectivo.
     *
     * @return JsonResponse
     * @Route("/grupo/listar", name="grupo_listar")
     */
    public function listarInformacionCampeonatoAction(): Response
    {
        $respuesta = array();
        $codigoRetorno = Response::HTTP_OK;

        $listarInformacionEquiposUseCase = new ListarInformacionEquiposInscritosUseCase(new GrupoDoctrineRepository($this->getDoctrine()));
        $informacionEquipos = $listarInformacionEquiposUseCase();

        if (isset($informacionEquipos)) {
            $respuesta = array("status" => "ok", "msg" => $informacionEquipos);
        } else {
            $respuesta = array("status" => "error", "msg" => "Error al cargar informacion.");
            $codigoRetorno = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return $this->json($respuesta, $codigoRetorno);
    }
}