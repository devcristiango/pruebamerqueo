<?php

namespace App\Controller\Campeonato;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\Campeonato\CampeonatoDoctrineRepository;
use App\Repository\Equipo\EquipoDoctrineRepository;
use App\Repository\Gol\GolDoctrineRepository;
use App\Repository\Partido\PartidoDoctrineRepository;
use App\Repository\ResultadoPartido\ResultadoPartidoDoctrineRepository;
use App\Repository\Tarjeta\TarjetaDoctrineRepository;
use App\Repository\Grupo\GrupoDoctrineRepository;
use App\UseCases\Campeonato\SimularCampeonatoUseCase;

class CampeonatoSimularController extends AbstractController
{
    /**
     * Esta acción se encarga de llamar al caso de uso que
     * simula los encuentros del mundial.
     *
     * @return JsonResponse
     * @Route("/campeonato/simular", name="campeonato_simular")
     */
    public function simularEjecucionCampeonato(): Response
    {
        $respuesta = array();
        $codigoRetorno = Response::HTTP_OK;

        $simularCampeonatoUseCase = new SimularCampeonatoUseCase(
            new CampeonatoDoctrineRepository($this->getDoctrine()),
            new EquipoDoctrineRepository($this->getDoctrine()),
            new GolDoctrineRepository($this->getDoctrine()),
            new GrupoDoctrineRepository($this->getDoctrine()),
            new PartidoDoctrineRepository($this->getDoctrine()),
            new ResultadoPartidoDoctrineRepository($this->getDoctrine()),
            new TarjetaDoctrineRepository($this->getDoctrine())
        );
        $resultadoCampeonato = $simularCampeonatoUseCase();

        if (isset($resultadoCampeonato)) {
            $respuesta = array("status" => "ok", "msg" => $resultadoCampeonato);
        } else {
            $respuesta = array("status" => "error", "msg" => "Error al cargar informacion.");
            $codigoRetorno = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return $this->json($respuesta, $codigoRetorno);
    }
}