<?php

namespace App\Controller\Campeonato;

use App\Repository\Campeonato\CampeonatoDoctrineRepository;
use App\UseCases\Campeonato\ObtenerTablaPosicionesUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CampeonatoTablaPosicionesController extends AbstractController
{
    /**
     * Esta funcionalidad retorna la tabla de posiciones del torneo
     * identificado por {id} o en su defecto del último campeonato.
     *
     * @param int|null $id
     * @return Response vista en twig de la tabla de posiciones
     * @Route("/campeonato/tabla/{id?}", name="campeonato_campeonato_tabla_posiciones")
     */
    public function index(?int $id): Response
    {
        $campeonatoId = 0;
        if (isset($id)) {
            $campeonatoId = $id;
        }

        $obtenerTablaPosicionesUseCase = new ObtenerTablaPosicionesUseCase(new CampeonatoDoctrineRepository($this->getDoctrine()));
        $informacionTablaPosiciones = $obtenerTablaPosicionesUseCase($campeonatoId);

        return $this->render('Campeonato/campeonato_tabla_posiciones/index.html.twig', [
            'informacionTablaPosiciones' => $informacionTablaPosiciones["posiciones"],
            'informacionTarjetas' => $informacionTablaPosiciones["tarjetas"],
            'informacionGoles' => $informacionTablaPosiciones["goles"],
            'informacionPartidosJugados' => $informacionTablaPosiciones["partidos"],
        ]);
    }
}
