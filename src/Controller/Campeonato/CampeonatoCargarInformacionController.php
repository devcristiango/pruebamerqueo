<?php

namespace App\Controller\Campeonato;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\Campeonato\CampeonatoDoctrineRepository;
use App\Repository\Grupo\GrupoDoctrineRepository;
use App\Repository\Equipo\EquipoDoctrineRepository;
use App\Repository\Jugador\JugadorDoctrineRepository;
use App\Services\Helpers\LectorCSV\LectorCSVPHP;
use App\UseCases\Campeonato\CargarInformacionEquiposDesdeCSVUseCase;

class CampeonatoCargarInformacionController extends AbstractController
{
    /**
     * Esta acción carga la vista principal del campeonato
     *
     * @Route("/campeonato/index", name="campeonato_index")
     */
    public function indexAction(Request $request): Response
    {
        return $this->render('Campeonato/index.html.twig');
    }

    /**
     * Esta acción carga la información de los equipos y
     * grupos.
     *
     * @return JsonResponse
     * @Route("/campeonato/cargar", name="campeonato_cargar")
     */
    public function cargarInformacionAction() : Response
    {
        $respuesta = array();
        $codigoRetorno = Response::HTTP_OK;

        $lectorCSV = new LectorCSVPHP();
        $objCargaInformacion = new CargarInformacionEquiposDesdeCSVUseCase(
            new CampeonatoDoctrineRepository($this->getDoctrine()),
            new GrupoDoctrineRepository($this->getDoctrine()),
            new EquipoDoctrineRepository($this->getDoctrine()),
            new JugadorDoctrineRepository($this->getDoctrine())
        );
        $projectRoot = $this->getParameter('kernel.project_dir');
        $direccionPath = $_ENV["DIR_CSV_DATASET"];

        $rutaPathArchivoCsv = $projectRoot . '/' . $direccionPath;

        $informacionCargada = $objCargaInformacion($lectorCSV, $rutaPathArchivoCsv);

        if ($informacionCargada) {
            $respuesta = array("status" => "ok", "msg" => "Informacion cargada exitosamente.");
        } else {
            $respuesta = array("status" => "error", "msg" => "Error al cargar informacion.");
            $codigoRetorno = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return $this->json($respuesta, $codigoRetorno);
    }

}
