<?php

namespace App\Services\Helpers\LectorCSV;

interface ILectorCSV
{
    public function leerCSV(string $path) : array;
}