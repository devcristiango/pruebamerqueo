<?php

namespace App\Services\Helpers\LectorCSV;

class LectorCSVPHP implements ILectorCSV
{

    public function __construct()
    {
    }

    /**
     * Esta función lee línea por línea el archivo CSV que se le envía
     * por parámetro y la devuelve en forma de array.
     *
     * @param string $path path en el sistema de archivos donde esta el csv file
     * @param bool $tieneCabecera indica si el csv tiene cabecera, para omitirla
     * @return array información leída del csv file
     */
    public function leerCSV(string $path, $tieneCabecera = true): array
    {
        $informacionCSV = array();
        if (file_exists($path) && is_file($path)) {
            $file = fopen($path, 'r');
            $flagCabecera = 0;
            while (($linea = fgetcsv($file)) !== FALSE) {
                if (!$flagCabecera && $tieneCabecera) {
                    $flagCabecera = 1;
                    continue;
                }
                $cantidadDatos = count($linea);
                if ($cantidadDatos > 0) {
                    array_push($informacionCSV, $linea);
                }
            }
            fclose($file);
        }
        return $informacionCSV;
    }
}