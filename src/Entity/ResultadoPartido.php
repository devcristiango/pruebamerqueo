<?php

namespace App\Entity;

use App\Repository\ResultadoPartidoDoctrineRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ResultadoPartidoDoctrineRepository::class)
 */
class ResultadoPartido
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $puntos;

    /**
     * @ORM\Column(type="boolean")
     */
    private $esGanador;

    /**
     * @ORM\ManyToOne(targetEntity=Partido::class, inversedBy="resultadoPartidos")
     */
    private $partido;

    /**
     * @ORM\ManyToOne(targetEntity=Equipo::class, inversedBy="resultadoPartidos")
     */
    private $equipo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPuntos(): ?int
    {
        return $this->puntos;
    }

    public function setPuntos(int $puntos): self
    {
        $this->puntos = $puntos;

        return $this;
    }

    public function getEsGanador(): ?bool
    {
        return $this->esGanador;
    }

    public function setEsGanador(bool $esGanador): self
    {
        $this->esGanador = $esGanador;

        return $this;
    }

    public function getPartido(): ?Partido
    {
        return $this->partido;
    }

    public function setPartido(?Partido $partido): self
    {
        $this->partido = $partido;

        return $this;
    }

    public function getEquipo(): ?Equipo
    {
        return $this->equipo;
    }

    public function setEquipo(?Equipo $equipo): self
    {
        $this->equipo = $equipo;

        return $this;
    }
}
