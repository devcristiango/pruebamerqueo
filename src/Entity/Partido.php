<?php

namespace App\Entity;

use App\Repository\PartidoDoctrineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PartidoDoctrineRepository::class)
 */
class Partido
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaHora;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fase;

    /**
     * @ORM\OneToMany(targetEntity=ResultadoPartido::class, mappedBy="partido")
     */
    private $resultadosPartido;

    /**
     * @ORM\ManyToOne(targetEntity=Equipo::class, inversedBy="partidosLocal")
     */
    private $equipoLocal;

    /**
     * @ORM\ManyToOne(targetEntity=Equipo::class, inversedBy="partidosVisitante")
     */
    private $equipoVisitante;

    /**
     * @ORM\ManyToOne(targetEntity=Campeonato::class, inversedBy="partidos")
     */
    private $campeonato;

    public function __construct()
    {
        $this->resultadosPartido = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaHora(): ?\DateTimeInterface
    {
        return $this->fechaHora;
    }

    public function setFechaHora(\DateTimeInterface $fechaHora): self
    {
        $this->fechaHora = $fechaHora;

        return $this;
    }

    public function getFase(): ?string
    {
        return $this->fase;
    }

    public function setFase(string $fase): self
    {
        $this->fase = $fase;

        return $this;
    }

    /**
     * @return Collection|ResultadoPartido[]
     */
    public function getresultadosPartido(): Collection
    {
        return $this->resultadosPartido;
    }

    public function addResultadoPartido(ResultadoPartido $resultadoPartido): self
    {
        if (!$this->resultadosPartido->contains($resultadoPartido)) {
            $this->resultadosPartido[] = $resultadoPartido;
            $resultadoPartido->setPartido($this);
        }

        return $this;
    }

    public function removeResultadoPartido(ResultadoPartido $resultadoPartido): self
    {
        if ($this->resultadosPartido->removeElement($resultadoPartido)) {
            // set the owning side to null (unless already changed)
            if ($resultadoPartido->getPartido() === $this) {
                $resultadoPartido->setPartido(null);
            }
        }

        return $this;
    }

    public function getEquipoLocal(): ?Equipo
    {
        return $this->equipoLocal;
    }

    public function setEquipoLocal(?Equipo $equipoLocal): self
    {
        $this->equipoLocal = $equipoLocal;

        return $this;
    }

    public function getEquipoVisitante(): ?Equipo
    {
        return $this->equipoVisitante;
    }

    public function setEquipoVisitante(?Equipo $equipoVisitante): self
    {
        $this->equipoVisitante = $equipoVisitante;

        return $this;
    }

    public function getCampeonato(): ?Campeonato
    {
        return $this->campeonato;
    }

    public function setCampeonato(?Campeonato $campeonato): self
    {
        $this->campeonato = $campeonato;

        return $this;
    }
}
