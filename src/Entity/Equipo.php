<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EquipoDoctrineRepository::class)
 */
class Equipo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bandera;

    /**
     * @ORM\OneToMany(targetEntity=Jugador::class, mappedBy="equipo")
     */
    private $jugadores;

    /**
     * @ORM\ManyToOne(targetEntity=Grupo::class, inversedBy="equipos")
     */
    private $grupo;

    /**
     * @ORM\OneToMany(targetEntity=Partido::class, mappedBy="equipoLocal")
     */
    private $partidosLocal;

    /**
     * @ORM\OneToMany(targetEntity=Partido::class, mappedBy="equipoVisitante")
     */
    private $partidosVisitante;

    /**
     * @ORM\OneToMany(targetEntity=ResultadoPartido::class, mappedBy="equipo")
     */
    private $resultadoPartidos;

    public function __construct()
    {
        $this->jugadores = new ArrayCollection();
        $this->partidosLocal = new ArrayCollection();
        $this->partidosVisitante = new ArrayCollection();
        $this->resultadoPartidos = new ArrayCollection();
    }


    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getBandera(): ?string
    {
        return $this->bandera;
    }

    public function setBandera(string $bandera): self
    {
        $this->bandera = $bandera;

        return $this;
    }

    /**
     * @return Collection|Jugador[]
     */
    public function getJugadores(): Collection
    {
        return $this->jugadores;
    }

    public function addJugador(Jugador $jugador): self
    {
        if (!$this->jugadores->contains($jugador)) {
            $this->jugadores[] = $jugador;
            $jugador->setEquipo($this);
        }

        return $this;
    }

    public function removeJugador(Jugador $jugador): self
    {
        if ($this->jugadores->removeElement($jugador)) {
            // set the owning side to null (unless already changed)
            if ($jugador->getEquipo() === $this) {
                $jugador->setEquipo(null);
            }
        }

        return $this;
    }

    public function getGrupo(): ?Grupo
    {
        return $this->grupo;
    }

    public function setGrupo(?Grupo $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * @return Collection|Partido[]
     */
    public function getPartidosLocal(): Collection
    {
        return $this->partidosLocal;
    }

    public function addPartidosLocal(Partido $partidosLocal): self
    {
        if (!$this->partidosLocal->contains($partidosLocal)) {
            $this->partidosLocal[] = $partidosLocal;
            $partidosLocal->setEquipoLocal($this);
        }

        return $this;
    }

    public function removePartidosLocal(Partido $partidosLocal): self
    {
        if ($this->partidosLocal->removeElement($partidosLocal)) {
            // set the owning side to null (unless already changed)
            if ($partidosLocal->getEquipoLocal() === $this) {
                $partidosLocal->setEquipoLocal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Partido[]
     */
    public function getPartidosVisitante(): Collection
    {
        return $this->partidosVisitante;
    }

    public function addPartidosVisitante(Partido $partidosVisitante): self
    {
        if (!$this->partidosVisitante->contains($partidosVisitante)) {
            $this->partidosVisitante[] = $partidosVisitante;
            $partidosVisitante->setEquipoVisitante($this);
        }

        return $this;
    }

    public function removePartidosVisitante(Partido $partidosVisitante): self
    {
        if ($this->partidosVisitante->removeElement($partidosVisitante)) {
            // set the owning side to null (unless already changed)
            if ($partidosVisitante->getEquipoVisitante() === $this) {
                $partidosVisitante->setEquipoVisitante(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ResultadoPartido[]
     */
    public function getResultadoPartidos(): Collection
    {
        return $this->resultadoPartidos;
    }

    public function addResultadoPartido(ResultadoPartido $resultadoPartido): self
    {
        if (!$this->resultadoPartidos->contains($resultadoPartido)) {
            $this->resultadoPartidos[] = $resultadoPartido;
            $resultadoPartido->setEquipo($this);
        }

        return $this;
    }

    public function removeResultadoPartido(ResultadoPartido $resultadoPartido): self
    {
        if ($this->resultadoPartidos->removeElement($resultadoPartido)) {
            // set the owning side to null (unless already changed)
            if ($resultadoPartido->getEquipo() === $this) {
                $resultadoPartido->setEquipo(null);
            }
        }

        return $this;
    }

}
