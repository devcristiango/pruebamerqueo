<?php

namespace App\Entity;

use App\Repository\TarjetaDoctrineRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TarjetaDoctrineRepository::class)
 */
class Tarjeta
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $minuto;

    /**
     * @ORM\ManyToOne(targetEntity=Jugador::class)
     */
    private $jugador;

    /**
     * @ORM\ManyToOne(targetEntity=ResultadoPartido::class)
     */
    private $resultadoPartido;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getMinuto(): ?string
    {
        return $this->minuto;
    }

    public function setMinuto(string $minuto): self
    {
        $this->minuto = $minuto;

        return $this;
    }

    public function getJugador(): ?Jugador
    {
        return $this->jugador;
    }

    public function setJugador(?Jugador $jugador): self
    {
        $this->jugador = $jugador;

        return $this;
    }

    public function getResultadoPartido(): ?ResultadoPartido
    {
        return $this->resultadoPartido;
    }

    public function setResultadoPartido(?ResultadoPartido $resultadoPartido): self
    {
        $this->resultadoPartido = $resultadoPartido;

        return $this;
    }
}
