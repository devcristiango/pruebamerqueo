<?php

namespace App\Entity;

use App\Repository\GolDoctrineRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GolDoctrineRepository::class)
 */
class Gol
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $minuto;

    /**
     * @ORM\ManyToOne(targetEntity=Jugador::class)
     */
    private $jugador;

    /**
     * @ORM\ManyToOne(targetEntity=ResultadoPartido::class)
     */
    private $resultadoPartido;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMinuto(): ?string
    {
        return $this->minuto;
    }

    public function setMinuto(string $minuto): self
    {
        $this->minuto = $minuto;

        return $this;
    }

    public function getJugador(): ?Jugador
    {
        return $this->jugador;
    }

    public function setJugador(?Jugador $jugador): self
    {
        $this->jugador = $jugador;

        return $this;
    }

    public function getResultadoPartido(): ?ResultadoPartido
    {
        return $this->resultadoPartido;
    }

    public function setResultadoPartido(?ResultadoPartido $resultadoPartido): self
    {
        $this->resultadoPartido = $resultadoPartido;

        return $this;
    }
}
