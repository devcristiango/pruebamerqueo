#Prueba Técnica Backend Merqueo II

Esta prueba consistió en crear un sistema que permitiera simular los 
encuentros de un campeonato mundial de fútbol.

##Instalación de proyecto
- Realice la clonación del presente proyecto.
- Prepare el siguiente entorno
  - En su sistema debe contar con php >=7.4.21
  - Motor de base de datos mysql 5.7 o mariadb compatible
    - Estos datos se deben configurar en el archivo .env
- Realice un composer install:
  - _composer install_
- Realice la creación y actualización del esquema de base de datos:
  - _php bin/console doctrine:database:create_
  - _php bin/console doctrine:schema:update --force_
- Se recomienda usar el symfony client server el cual puede ser obtenido de 
forma gratuita en https://symfony.com/download.
  - Una vez instalado puede servir la aplicación con:
    - _symfony server:start_
  
###Preparación para entorno de pruebas
- Este desarrollo usa PHPUnit como herramienta de pruebas unitarias. Para 
usarlas es necesario crear el esquema de base de datos el cual usará
doctrine como base de testing.
  - El esquema se encuentra en la raíz del proyecto con nombre _dbTest.sql_
  - El archivo csv de prueba se aloja en la carpeta public/equipos.csv
  - Para la ejecución de los test usar
    - _php bin/phpunit_


##Estructura del Proyecto
Este proyecto está estructurado de la siguiente forma, la cual se basa en un enfoque 
una clean arquitecture tipo arquitectura hexagonal, el cual favoreció la
aplicación de los principios de inversión de dependencias, responsabilidad
única e inyección de dependencias.

Dentro de la carpeta src/:
- _Controller:_ Esta carpeta contiene los controladores de la aplicación
los cuales interactuan con los casos de uso enviandoles la información
preparada y retornando los valores. Internamente almacena los controladores
del dominio en sus respectivas carpetas.
- _Entity:_ Contiene las clases del dominio.
- _Repository:_ Contiene las interfaces y las clases concretas de los
repositorios de las entidades. Internamente se organiza por su dominio.
- _Services:_ Contiene los helpers tanto interfaces como clases concretas.
- _UseCases:_ Contiene los casos de uso. Organizados internamente por
cada entidad del dominio.

Dentro de la carpeta tets/:
- Se organizan en forma de 'espejo' de la carpeta _src_.


##Modelo de dominio

Se plantea el siguiente modelo de dominio:

![Modelo de dominio](diseno/ModeloDominio.png "Modelo de dominio")

##Modelo de base de datos

Se plantea el siguiente modelo:

![Modelo de BBDD](diseno/EsquemaBaseDatos.png "Modelo de base de datos")


#Aplicacion en Ejecución

Luego de montada la aplicación dirigirse a las siguientes url:

- http://localhost:8000/campeonato/index
  - En esta página encontrará el paso a paso de la simulación.
- http://localhost:8000/campeonato/tabla/
  - En esta página encontrará los resultados de la simulación actual.
- http://localhost:8000/campeonato/tabla/{id}
  - Se puede utilizar para consultar emulaciones anteriores.

#Nota final

Esta aplicación se desarrolla con fines de la prueba técnica, el 
desarrollador no se hace responsable de su uso en entornos de producción
o similares. 

###Gracias! :D
